import argparse
import json
import os

from sklearn.calibration import CalibratedClassifierCV
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.pipeline import FeatureUnion
from sklearn.preprocessing import MaxAbsScaler
from sklearn.svm import LinearSVC

from features import Lix, WordSizes, CharUpperLowerRatio, CountWordCaps, Lexical, PunctCounter


def fit_vectorizers_on_corpus(corpus_path, collection_info):
    problem_dict = {'en': [], 'fr': [], 'it': [], 'sp': []}
    with open(collection_info, 'r') as infile:
        collection_info_json = json.load(infile)

    for problem in collection_info_json:
        problem_dict[problem['language']] += [problem['problem-name']]

    print('Fitting Vectorizers for each language..')
    for language, problems in problem_dict.items():
        print(language)

        func_word_vectorizer = TfidfVectorizer(analyzer='word', ngram_range=(1, 3), min_df=2, lowercase=True,
                                               use_idf=True, smooth_idf=True)
        char_vectorizer = TfidfVectorizer(analyzer='char', ngram_range=(1, 3), min_df=25, lowercase=True, use_idf=True,
                                          smooth_idf=True)

        # pass in 'language' to the Feature Objects
        vectorizer = FeatureUnion([
            ('asd', CountVectorizer(analyzer='char', ngram_range=(1, 4), binary=True)),
            ('characters', char_vectorizer),
            ('Lix', Lix()),
            ('Lexical', Lexical()),
            ('CountWordCaps', CountWordCaps()),
            ('CharUpperLowerRatio', CharUpperLowerRatio()),
            ('function_words', func_word_vectorizer),
            ('punct', PunctCounter()),
            ('WordSizes', WordSizes()),
        ])

        for problem in problems:
            language_corpus = []
            authors = sorted([author for author in os.listdir(os.path.join(corpus_path, problem)) if
                              'candidate' in author or 'unk' in author])  # uncomment unk to build vocab without unks
            for author in authors:
                author_file_paths = sorted(os.listdir(os.path.join(corpus_path, problem, author)))
                for author_file in author_file_paths:
                    with open(os.path.join(corpus_path, problem, author, author_file)) as infile:
                        text = infile.read()
                        language_corpus.append(text)

            vectorizer.fit(language_corpus)
            joblib.dump(vectorizer, os.path.join('models', 'vectorizer_{}.pkl'.format(problem)))
    print('Vectorizers fitted for all languages..')


def train_and_predict(corpus_path, collection_info, outpath):
    problem_dict__ = {'en': [], 'fr': [], 'it': [], 'sp': []}
    with open(collection_info, 'r') as infile:
        collection_info_json = json.load(infile)

    for problem in collection_info_json:
        problem_dict__[problem['language']] += [problem['problem-name']]

    for language, problems in problem_dict__.items():
        print(language)
        print('Training classifiers for each problem..')

        for problem in problems:
            vectorizer = joblib.load(os.path.join('models', 'vectorizer_{}.pkl'.format(problem)))
            print(problem, '..', language)
            problem_path = os.path.join(corpus_path, problem)
            candidates = sorted([file for file in os.listdir(problem_path) if 'cand' in file])

            X, y = [], []

            for candidate in candidates:
                candidate_path = os.path.join(corpus_path, problem, candidate)
                candidate_texts = sorted([file for file in os.listdir(candidate_path)])

                for candidate_text in candidate_texts:
                    with open(os.path.join(corpus_path, problem, candidate, candidate_text), 'r',
                              encoding='UTF-8') as cand_text_file:
                        vec = vectorizer.transform([cand_text_file.read()]).todense().tolist()
                        X += vec
                        y.append(candidate)

            transformer = MaxAbsScaler().fit(X)
            X = transformer.transform(X)
            clf = CalibratedClassifierCV(OneVsRestClassifier(LinearSVC(C=1.0)))
            clf.fit(X, y)

            unk_files_per_problem = sorted(os.listdir(os.path.join(corpus_path, problem, 'unknown')))
            solutions = []
            for unk_file in unk_files_per_problem:
                unk_file_path = os.path.join(corpus_path, problem, 'unknown', unk_file)
                with open(unk_file_path, 'r') as unk_file_in:
                    vec = transformer.transform(vectorizer.transform([unk_file_in.read()]).todense().tolist())
                    predictions = clf.predict_proba(vec)
                    sorted_predictions = sorted(predictions[0])

                    if (sorted_predictions[-1] - sorted_predictions[-2]) >= 0.10:
                        solution = {
                            "unknown-text": unk_file,
                            "predicted-author": clf.predict(vec)[0]
                        }
                    else:
                        solution = {
                            "unknown-text": unk_file,
                            "predicted-author": "<UNK>"
                        }

                solutions.append(solution)

            """with open(os.path.join('..', 'data/pan19-answers-custom', 'answers-' + problem + '.json'), 'w') as fout:
                json.dump(solutions, fout)"""

            with open(os.path.join(outpath, 'answers-' + problem + '.json'), 'w') as fout:
                json.dump(solutions, fout)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", help='path to input / training data')
    parser.add_argument("-o", help='path to output folder')

    args = parser.parse_args()

    _inpath = args.i
    _outpath = args.o

    if _inpath is None:
        print('inpath is empty')
        exit()
    if _outpath is None:
        print('outpath is empty')
        exit()

    # corpus_path = os.path.join('..', 'data/pan19-cdaa-development-corpus')
    # collection_info = os.path.join(corpus_path, 'collection-info.json')

    _corpus_path = _inpath
    _collection_info = os.path.join(_corpus_path, 'collection-info.json')

    fit_vectorizers_on_corpus(corpus_path=_corpus_path, collection_info=_collection_info)
    train_and_predict(corpus_path=_corpus_path, collection_info=_collection_info, outpath=_outpath)
