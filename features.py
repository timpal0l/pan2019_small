from entropy import shannon_entropy

import numpy as np
from nltk.tokenize import word_tokenize, sent_tokenize
from scipy import sparse
from sklearn.base import BaseEstimator, TransformerMixin


class Lix(BaseEstimator, TransformerMixin):
    def fit(self, X, y=None):
        return self

    def transform(self, X):
        raw_text = X[0]
        words = word_tokenize(raw_text)
        sents = sent_tokenize(raw_text)
        try:
            number_of_words = len(words)
            number_of_sents = len(sents)
            long_words = sum([1 for word in words if len(word) > 6])
            lix = (number_of_words / number_of_sents) + ((long_words * 100) / number_of_words)
        except ZeroDivisionError:
            lix = 0.0

        return sparse.csr_matrix(lix)

    def get_feature_names(self):
        return ["lix"]  # rename to lix


class CharUpperLowerRatio(BaseEstimator, TransformerMixin):
    """
        Model that extracts the ratio between upper and lower chars
    """

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        text = X[0]
        chars_upper = sum(1 for c in text if c.isupper())
        chars_lower = sum(1 for c in text if c.islower())
        try:
            upper_lower_ratio = chars_upper / chars_lower
        except ZeroDivisionError:
            upper_lower_ratio = .0

        return sparse.csr_matrix([upper_lower_ratio])

    def get_feature_names(self):
        return ["char_upper_lower_ratio"]


class CountWordCaps(BaseEstimator, TransformerMixin):
    """
        Model that extracts a counter of capital words from text.
        Also normalises to token ratio to adjust for text length
    """

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        raw_text = word_tokenize(X[0])
        return sparse.csr_matrix([sum(w.isupper() for w in raw_text) / len(raw_text)])

    def get_feature_names(self):
        return ["number_of_capital_words"]


class Lexical(BaseEstimator, TransformerMixin):
    """
    Counts the occurrences of various Lexical features;
    averages of sentence length & word length,
    std of sentence and word length,
    lexical diverity (vocabulary richness),
    also shannon entropy of a string.
    Also normalises to token ratio to adjust for text length
    """

    def fit(self, X, y=None):
        return self

    def transform(self, X):

        raw_text = X[0]
        sents = sent_tokenize(raw_text)
        words = word_tokenize(raw_text)
        vocab = set(words)
        words_per_sentence = np.array([len(word_tokenize(sent)) for sent in sents])
        avg_word_len = np.array([len(word) for word in words]).mean()
        std_word_len = np.array([len(word) for word in words]).std()
        avg_sen_len = words_per_sentence.mean()
        std_sen_len = words_per_sentence.std()
        try:
            lex_diversity = len(vocab) / float(len(words))
        except Exception:
            lex_diversity = .0

        lexical = np.zeros(6)
        lexical[0] = avg_sen_len
        lexical[1] = std_sen_len
        lexical[2] = lex_diversity
        lexical[3] = avg_word_len
        lexical[4] = std_word_len
        lexical[5] = shannon_entropy(raw_text)

        return sparse.csr_matrix(lexical)

    def get_feature_names(self):
        return ["avg_sen_len", "std_sen_len", "lex_diversity", "avg_word_len", "std_word_len", "entropy"]


class PunctCounter(BaseEstimator, TransformerMixin):
    """
    Counts the occurrences of various Punctuations
    Also normalises to token ratio to adjust for text length
    """

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        trailings = X[0].count('...')
        x = word_tokenize(X[0])
        punctations = list(""".,?!;:()[]{}@#´`'"-_\/|%~*+-""")
        vector = np.zeros(len(punctations) + 1)
        col = 0

        for punct in punctations:
            vector[col] = x.count(punct) / len(x)
            if col == len(punct) - 1:
                col = 0
            col += 1

        vector[col] = trailings / len(x)

        return sparse.csr_matrix(vector)

    def get_feature_names(self):
        return list(""".,?!;:()[]{}@#´`'"-_\/|%~*+-""") + ["..."]


class WordSizes(BaseEstimator, TransformerMixin):
    """
    Counts the occurrences of word lengths list(range(1,16))
    Also normalises to token ratio to adjust for text length
    """

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        tokens = word_tokenize(X[0])
        wl_array = np.zeros(15)

        for word in tokens:
            word_len = len(word)
            if 0 < word_len <= 15:
                wl_array[word_len - 1] += 1

        wl_array = np.divide(wl_array, len(tokens))
        return sparse.csr_matrix(wl_array)

    def get_feature_names(self):
        return [f"size_{e+1}" for e, x in enumerate(np.zeros(15))]
